#!/usr/bin/env groovy

import com.script.Docker

def call(){
    return new Docker(this).dockerLogin()
}