#!/usr/bin/env groovy

import com.script.Docker

def call(String imageName){
    return new Docker(this).dockerPush(imageName)
}